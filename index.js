fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  })

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => console.log(data.completed, data.title))

fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "New To Do Item",
    completed: false
  })
}).then(response => response.json())
  .then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Updated To Do Item",
    completed: true
  })
}).then(response => response.json())
  .then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Updated To Do Item",
    description: "This is the updated description.",
    status: "completed",
    dateCompleted: "2023-02-13",
    userId: 1
  })
}).then(response => response.json())
  .then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Patched To Do Item"
  })
}).then(response => response.json())
  .then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    status: "complete",
    dateCompleted: "2023-02-13"
  })
}).then(response => response.json())
  .then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
}).then(response => console.log(response))